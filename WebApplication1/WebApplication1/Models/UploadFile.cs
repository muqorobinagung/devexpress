﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class UploadFile
    {
        [DisplayName("Upload File")]
        public string filename { get; set; }

        public HttpPostedFileBase FileDocument { get; set; }
    }
}