//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SystemParameter
    {
        public int PK_SystemParameter_ID { get; set; }
        public Nullable<int> FK_SystemParameterGroup_ID { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<bool> Hide { get; set; }
        public Nullable<int> fk_MFieldType_ID { get; set; }
        public Nullable<bool> IsEncript { get; set; }
        public string EncriptionKey { get; set; }
    }
}
