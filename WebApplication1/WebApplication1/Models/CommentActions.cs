﻿using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class CommentActions
    {
        RichEditDocumentServer wordProcessor = new RichEditDocumentServer();
        public void CreateComment()
        {
            #region #CreateComment
            Document document = wordProcessor.Document;            
            wordProcessor.LoadDocument("D:/NAWADATA/TALENT POOL/Project_Lain/WebApplication1/WebApplication1/Content/Test.docx",DocumentFormat.OpenXml);
            DocumentRange docRange = document.Paragraphs[0].Range;
            string commentAuthor = "Muqorobin";
            document.Comments.Create(docRange, commentAuthor, DateTime.Now);
            
            wordProcessor.SaveDocument("D:/NAWADATA/TALENT POOL/Project_Lain/WebApplication1/WebApplication1/Content/Test.docx", DocumentFormat.OpenXml);
            #endregion #CreateComment
        }

        public List<string> GetAuthorList(string filePath)
        {
            //if (string.IsNullOrEmpty(filePath))
            //{
            //    throw new Exception("cannot be null");
            //}

            //if (!File.Exists(filePath))
            //{
            //    throw new Exception("File not exist");
            //}

            Document document = wordProcessor.Document;
            wordProcessor.LoadDocument(filePath);
            CommentCollection commentsCollection = document.Comments;
            int commentNum = commentsCollection.Count;
            List<string> Author = new List<string>();
            foreach (var obj in commentsCollection)
            {
                string auth = obj.Author;
                Author.Add(auth);
            }
            return Author;
        }

        public List<string> GetCommentList()
        {
            Document document = wordProcessor.Document;
            wordProcessor.LoadDocument("D:/NAWADATA/TALENT POOL/Project_Lain/WebApplication1/WebApplication1/Content/Test.docx", DocumentFormat.OpenXml);

            List<string> Commentsss = new List<string>();
            int commentNum = document.Comments.Count;
            int i = 0;
            while (i < commentNum)
            {
                DevExpress.XtraRichEdit.Model.PieceTable objPieceTable = ((DevExpress.XtraRichEdit.API.Native.Implementation.NativeComment)document.Comments[i]).Comment.Content.PieceTable;
                string text = objPieceTable.TextBuffer.ToString();
                Commentsss.Add(text);
                i++;
            }
            return Commentsss;
        }
    }
}