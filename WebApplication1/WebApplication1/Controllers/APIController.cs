﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DevExpress.Web.Demos
{
    public partial class APIController : Controller
    {
        public ActionResult ClientSideEvents()
        {
            ViewData["ClientSideEvents"] = new string[] {
                "ActiveSubDocumentChanged",
                "BeginSynchronization",
                "CharacterPropertiesChanged",
                "DocumentChanged",
                "DocumentLoaded",
                "EndSynchronization",
                "HyperlinkClick",
                "Init",
                "KeyDown",
                "KeyUp",
                "PointerDown",
                "PointerUp",
                "LostFocus",
                "GotFocus",
                "ParagraphPropertiesChanged",
                "PopupMenuShowing",
                "SelectionChanged",
                "DocumentFormatted",
                "ContentInserted",
                "ContentRemoved"
            };
            ViewData["ShowEventListPanel"] = true;
            return View("ClientSideEvents");
        }
        public ActionResult ClientSideEventsPartial()
        {
            return PartialView("ClientSideEventsPartial");
        }

        public ActionResult DocumentChanged()
        {
            return PartialView("_RichEditPartial");
        }
    }
}