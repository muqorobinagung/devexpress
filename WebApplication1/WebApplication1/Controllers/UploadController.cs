﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(UploadFile file)
        {
            string message = "";
            using (talentPoolEntities db = new talentPoolEntities())
            {
                string uname = Session["UserName"].ToString();
                if (file != null)
                {
                    string FileName = Path.GetFileNameWithoutExtension(file.FileDocument.FileName);
                    string FileExtension = Path.GetExtension(file.FileDocument.FileName);
                    string FullFileName = FileName + FileExtension;
                    string FilePath = Path.Combine(Server.MapPath("~/Documents/"), FullFileName);
                    DateTime datenow = DateTime.Now;

                    file.FileDocument.SaveAs(FilePath);

                    tbl_files newFile = new tbl_files();
                    newFile.files_name = FileName;
                    newFile.files_ext = FileExtension;
                    newFile.files_str = FullFileName;
                    newFile.uploaddate = datenow;
                    newFile.uploadby = uname;

                    db.tbl_files.Add(newFile);
                    db.SaveChanges();

                    message = "File successfully uploaded. " + FullFileName + " uploaded by " + uname + " at " + datenow.ToString();
                }
                else
                {
                    message = "No file uploaded";
                }
            }
            ViewBag.Message = message;
            return View();
        }
    }
}