﻿using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WebApplication1.Models;
using NawaBLL;
using NawaDAL;
using System.Web.Security;
using System.IO;
using DevExpress.Web;
using System.Web.UI;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {

        public string filePath { get; set; }
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return RedirectToAction("ClientSideEvents", "API");
        }

        public ActionResult Contact()
        {
            //CommentActions commentActions = new CommentActions();
            //commentActions.CreateComment();

            //List<string> AuthorList = commentActions.GetAuthorList();
            //List<string> CommentList = commentActions.GetCommentList();
            //ViewBag.Message = "Your contact page.";
            //ViewBag.AuthorList = AuthorList;
            //ViewBag.CommentList = CommentList;         
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(NawaDAL.MUser login)
        {
            string message = "";
            using (talentPoolEntities dc = new talentPoolEntities())
            {
                //int i = ((int)NawaBLL.SystemParameterBLL.SytemParameterEnum.LoginUsedIPAddress);
                //int objsysparam = Convert.ToInt32(NawaBLL.SystemParameterBLL.GetSystemParameterByPk((int)NawaBLL.SystemParameterBLL.SytemParameterEnum.LoginUsedIPAddress));
                //Dim objsysparam As NawaDAL.SystemParameter = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(NawaBLL.SystemParameterBLL.SytemParameterEnum.LoginUsedIPAddress)
                //bool IsCheckIPAddress = true;

                
                var v = dc.MUsers.Where(a => a.UserID == login.UserID).FirstOrDefault();
                if (v != null)
                {
                    string salt = v.UserPasswordSalt; //read from database
                    string PlainPass = login.UserPasword;
                    if (PlainPass != null)
                    {
                        string a = NawaBLL.Common.Encrypt(PlainPass, salt);
                        if (a == v.UserPasword)
                        {
                            Session["PK_MUser_ID"] = v.PK_MUser_ID.ToString();
                            Session["UserID"] = v.UserID.ToString();
                            Session["UserName"] = v.UserName.ToString();
                            Session["UserEmailAddress"] = v.UserEmailAddress.ToString();
                            Session["FK_MGroupMenu_ID"] = v.FK_MGroupMenu_ID.ToString();
                            Session["FK_MRole_ID"] = v.FK_MRole_ID.ToString();
                            return RedirectToAction("Index", "HomeDevExpress");
                        }
                        else
                        {
                            message = "Invalid credential provided";
                        }
                    }

                }
                else
                {
                    message = "Invalid credential provided";
                }
            }
            ViewBag.Message = message;
            return View();
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            Session.RemoveAll();
            Response.Cookies.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult HtmlEditorPartial()
        {
            return PartialView("_HtmlEditorPartial");
        }
        //public ActionResult HtmlEditorPartialImageSelectorUpload()
        //{
        //    HtmlEditorExtension.SaveUploadedImage("HtmlEditor", HomeControllerHtmlEditorSettings.ImageSelectorSettings);
        //    return null;
        //}
        //public ActionResult HtmlEditorPartialImageUpload()
        //{
        //    HtmlEditorExtension.SaveUploadedFile("HtmlEditor", HomeControllerHtmlEditorSettings.ImageUploadValidationSettings, HomeControllerHtmlEditorSettings.ImageUploadDirectory);
        //    return null;  
        //}

        public ActionResult RichEdit1Partial()
        {
            return PartialView("_RichEdit1Partial");
        }

        //upload
        public ActionResult UploadControlUpload()
        {
            string[] errors;

            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("UploadControl",
                CustomUploadControlValidationSettings.Settings, out errors,
                UploadControl_FileUploadComplete,
                UploadControl_FilesUploadComplete);

            string resultFilePath = "~/Content/" + files[0].FileName;

            //filePath = Server.MapPath(files[0].FileName);
            filePath = System.Web.HttpContext.Current.Request.MapPath(resultFilePath);
            RichEdit2Partial(filePath);

            return null;

        }

        public void UploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {

        }

        public void UploadControl_FilesUploadComplete(object sender, FilesUploadCompleteEventArgs e)
        {
            UploadedFile[] files = ((MVCxUploadControl)sender).UploadedFiles;
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].IsValid && !string.IsNullOrWhiteSpace(files[i].FileName))
                {
                    string resultFilePath = "~/Content/" + files[i].FileName;
                    files[i].SaveAs(System.Web.HttpContext.Current.Request.MapPath(resultFilePath));

                    IUrlResolutionService urlResolver = sender as IUrlResolutionService;
                    if (urlResolver != null)
                        e.CallbackData += urlResolver.ResolveClientUrl(resultFilePath);
                }
            }

        }

        public ActionResult RichEdit2Partial(string path)
        {
            ViewData["path"] = path; 
            CommentActions commentActions = new CommentActions();
            //commentActions.CreateComment();
        
            List<string> AuthorList = commentActions.GetAuthorList(filePath);
            List<string> CommentList = commentActions.GetCommentList();
            ViewBag.Message = "Your contact page.";
            ViewBag.AuthorList = AuthorList;
            ViewBag.CommentList = CommentList;
            return PartialView("_RichEdit2Partial");
        }

        public ActionResult CallbackPanelPartial(string path)
        {
            ViewData["path"] = path;
            //wordProcessor.LoadDocument(path, DocumentFormat.OpenXml);
            //wordProcessor.BeginUpdate();
            //Document document = wordProcessor.Document;
            //DocumentPosition pos = document.Range.Start;
            //document.Bookmarks.Create(wordProcessor.Document.CreateRange(pos, 0), "Top");
            ////Insert the hyperlink anchored to the created bookmark:
            //DocumentPosition pos1 = document.CreatePosition((wordProcessor.Document.Range.End).ToInt() + 25);
            //document.Hyperlinks.Create(wordProcessor.Document.InsertText(pos1, "get to the top"));
            //document.Hyperlinks[0].Anchor = "Top";
            //wordProcessor.EndUpdate();
            return PartialView("_CallbackPanelPartial");
        }

        

    }
    //public class HomeControllerHtmlEditorSettings
    //{
    //    public const string ImageUploadDirectory = "~/Content/UploadImages/";
    //    public const string ImageSelectorThumbnailDirectory = "~/Content/Thumb/";

    //    public static DevExpress.Web.UploadControlValidationSettings ImageUploadValidationSettings = new DevExpress.Web.UploadControlValidationSettings()
    //    {
    //        AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".jpe", ".gif", ".png" },
    //        MaxFileSize = 4000000
    //    };

    //    static DevExpress.Web.Mvc.MVCxHtmlEditorImageSelectorSettings imageSelectorSettings;
    //    public static DevExpress.Web.Mvc.MVCxHtmlEditorImageSelectorSettings ImageSelectorSettings
    //    {
    //        get
    //        {
    //            if (imageSelectorSettings == null)
    //            {
    //                imageSelectorSettings = new DevExpress.Web.Mvc.MVCxHtmlEditorImageSelectorSettings(null);
    //                imageSelectorSettings.Enabled = true;
    //                imageSelectorSettings.UploadCallbackRouteValues = new { Controller = "Home", Action = "HtmlEditorPartialImageSelectorUpload" };
    //                imageSelectorSettings.CommonSettings.RootFolder = ImageUploadDirectory;
    //                imageSelectorSettings.CommonSettings.ThumbnailFolder = ImageSelectorThumbnailDirectory;
    //                imageSelectorSettings.CommonSettings.AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".jpe", ".gif" };
    //                imageSelectorSettings.UploadSettings.Enabled = true;
    //            }
    //            return imageSelectorSettings;
    //        }
    //    }
    //}

}