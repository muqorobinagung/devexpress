﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using DevExpress.Web;
namespace DevExpress.Web.Demos
{
    public static class RichEditDemoUtils
    {
        public static void HideFileTab(ASPxRichEdit.ASPxRichEdit richedit)
        {
            richedit.CreateDefaultRibbonTabs(true);
            RibbonTab fileTab = richedit.RibbonTabs[0];
            richedit.RibbonTabs.Remove(fileTab);
        }
    }
}

